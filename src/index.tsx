import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import App from './App';
import * as serviceWorker from './serviceWorker';
import {combineReducers, createStore} from "redux";
import {usernameReducer} from "./redux/reducers/UsernameReducer";

// The root reducer is the 'root' of the Redux state. All semantically different reducers should be combined here.
// Here, we obviously only have one reducer so our Redux state will look like this: { usernameReducer: usernameReducer }
const rootReducer = combineReducers({

    usernameReducer

});

// Create the Redux store with our root reducer
// Basically, create the Redux store with all potential ways we can modify it.
const store = createStore(rootReducer);


ReactDOM.render(

    <Provider store={store}>
          <React.StrictMode>
            <App />
          </React.StrictMode>
    </Provider>,

  document.getElementById('root')

);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
