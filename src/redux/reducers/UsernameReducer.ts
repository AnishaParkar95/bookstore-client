
import {IUsernameState} from "../states/IUsernameState";
import {AnyAction} from "redux";
import {EUsernameActions} from "../actions/UsernameActions";


// For our default state, let's start with a default username.
// This is kind of similar to doing defaults with React.useState('Default username')
const DEFAULT_STATE: IUsernameState = {

    // username: 'HI'

};

// Redux reducers describe how the state should change when given a message.
// Our only message for right now is basically "HEY REDUX! CHANGE THE USERNAME".
// Here, we will define how that should happen.
export function usernameReducer(state = DEFAULT_STATE, action: AnyAction): IUsernameState {

    /**
     * Just for reference, here is our message (action) from UsernameActions:
     *
     *     return {
     *         type: EUsernameActions.CHANGE_USERNAME,
     *         username: username
     *     }
     *
     */

    // Look at the message above, our action type is one of the EUsernameActions enums.
    // We can handle many types of actions, so switch on the enums
    switch (action.type) {

        // This is why I said to use enums, no concern of mistyping a string here!
        case EUsernameActions.CHANGE_USERNAME:

            // How should our state change with this action? Keep everything else in the state,
            // which is literally nothing because we only have one variable, and overwrite username with the
            // username supplied by the action
            return { ...state, username: action.username };

        // This is a Redux thing, when your store is built, Redux will essentially send a bogus action
        // though to construct your default state. You must return the state here as a default for this.
        default:
            return state;

    }
}

