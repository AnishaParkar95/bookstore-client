import {IUsernameState} from "./states/IUsernameState";


export interface IRootReducer {

    usernameReducer: IUsernameState

}