import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Box from '@material-ui/core/Box';
import NavBar from "./components/NavBar";
import {TextField, Typography} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Rating from '@material-ui/lab/Rating';
import Axios from "axios";
const useStyles = makeStyles((myTheme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: myTheme.spacing(2),
            textAlign: 'left',
            color: myTheme.palette.text.secondary,
        },
        imgPaper:{
            align:'right'
        },
        textField: {
            marginLeft: myTheme.spacing(1),
            marginRight: myTheme.spacing(1),
            width: 200,
        },
    }),
);

const bookpost = {
    title: 'Becoming',
    description: "An intimate, powerful, and inspiring memoir by the former First Lady of the United States.",
    price: "27.49",
    image: "https://www.statefarmarena.com/assets/img/MichelleObama-590x590-StateFarm-56b2d1571b.jpg"

};

export default function BookDetailPage() {

    const classes = useStyles();

    const [orderQuantity, setOrderQuantity] = React.useState('');


    async function buyBook(event:any) {

        event.preventDefault();

        try{

            const data = {bookName: bookpost.title, bookPrice:bookpost.price, orderQuantity: orderQuantity}

            const response = await Axios.post('/orderBook', data);

            console.log(response);

        }catch (error) {

            console.log(error.message)

        }
    }


    const [value, setValue] = React.useState<number | null>(2);
    return (
        <Grid container spacing={0}>
            <Grid item xs={12}>
                <Paper className={classes.paper} elevation={0}>
                    <NavBar/>
                </Paper>
            </Grid>
            <Grid item xs={12} sm={6}>
                <Paper className={classes.imgPaper} elevation={0}>
                    <img src= {bookpost.image} alt={''}/>
                </Paper>
            </Grid>
            <Grid item xs={12} sm={6}>
                <Paper className={classes.paper} elevation={0}>
                    <Typography variant="h3" gutterBottom>
                        {bookpost.title}
                    </Typography>
                    <Typography variant="h5" gutterBottom>
                        {bookpost.description}
                    </Typography>
                    <Typography variant="h5" gutterBottom>
                        ${bookpost.price}
                    </Typography>
                    <Box component="fieldset" mb={3} borderColor="transparent">
                        <Typography component="legend">Feel Free to Rate </Typography>
                        <Rating
                            name="simple-controlled"
                            value={value}
                            onChange={(event, newValue) => {
                                setValue(newValue);
                            }}
                        />
                    </Box>
                    <Box  component="fieldset" mb={3} borderColor="transparent">

                        <TextField
                            id="outlined-number"
                            label="Quantity"
                            type="number"
                            InputLabelProps={{shrink: true,}}
                            variant="outlined"
                            value ={orderQuantity}
                            onChange={(event => setOrderQuantity(event.target.value) )}

                        />
                    </Box>

                 <Button variant= "contained" color="primary"
                         style={{marginLeft:'3rem'}}
                         onClick={buyBook}>
                     Buy
                 </Button>
                </Paper>
            </Grid>
        </Grid>
    );
}

BookDetailPage.propTypes = {
    post: PropTypes.object,
};