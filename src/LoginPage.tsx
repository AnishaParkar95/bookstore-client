
import React from 'react';
import Axios from 'axios';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import TextField from "@material-ui/core/TextField/TextField";
import {useHistory} from "react-router";
import {useDispatch} from "react-redux";
// import {IRootReducer} from "./redux/IRootReducer";
import {changeUsername} from "./redux/actions/UsernameActions";

// import EmailAddressInput from "./components/EmailAddressInput";
// import PasswordInput from "./components/PasswordInput";

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export  function LoginPage() {
    const classes = useStyles();
    const history = useHistory();

    // Hook used to 'dispatch' changes to a Redux store (i.e. change the state of)
    const dispatch = useDispatch();

    // Hook used to pick a specific state out of the Redux store
    // Note: The selector is given the root state, we must dig down to the specific property we care about
    // const username: string | undefined = useSelector<IRootReducer, string | undefined>(
    //     state => state.usernameReducer.username);


    const [emailID, setEmailID] = React.useState('');
    const [ password, setPassword] = React.useState('');

    async function Login(event:any) {

        event.preventDefault();

        console.log("I am here");

        console.log(emailID);
        console.log(password);

        try{

            const data = {emailAddress: emailID, password : password}

           const response = await Axios.post('/login', data);

            console.log(response);

            history.push("/");

            const username: string = "Hi,"+emailID;
            dispatch(changeUsername(username));


        }catch (error) {

            console.log(error.message)

        }

    }

    return (
        <div>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <form className={classes.form} noValidate>

                        {/*<EmailAddressInput emailAddress = {emailID} onEmailIDChange={(event) => setEmailID(event.target.value)}/>*/}

                        {/*<PasswordInput/>*/}

                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            autoFocus
                            value = {emailID}
                            onChange={(event => setEmailID(event.target.value) )}
                        />

                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            autoComplete="current-password"
                            value = {password}
                            onChange={(event) => setPassword(event.target.value)}
                        />

                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="secondary"
                            className={classes.submit}
                            onClick={Login}
                        >
                            Sign In
                        </Button>
                        <Grid container>
                            <Grid item>
                                <Link href="/register" variant="body2">
                                    {"Don't have an account? Sign Up"}
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
                <Box mt={8}>
                    <Copyright />
                </Box>
            </Container>
        </div>
    );
}