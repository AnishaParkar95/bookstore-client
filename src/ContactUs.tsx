import React from "react";
import Typography from '@material-ui/core/Typography';
import { TextField, Divider, Grid } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import {makeStyles} from "@material-ui/core/styles";
import NavBar from "./components/NavBar";
import Axios from "axios";
import {useHistory} from "react-router";



const useStyles = makeStyles((theme) => ({
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing(10),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(8),
            marginBottom: theme.spacing(4),
            padding: theme.spacing(3),
        },
    },
}));

export default function ContactUs(){

    const classes = useStyles();
    const history = useHistory();

    const [senderEmail, setSenderEmailID] = React.useState('');
    const [emailSubject, setEmailSubject] = React.useState('');
    const [emailText, setEmailText] = React.useState('');

    async function sendEmail(event:any) {

        event.preventDefault();

        try{

            const data = {senderEmail: senderEmail, emailSubject: emailSubject, emailText: emailText}

            const response = await Axios.post('/contactUs', data);

            console.log(response);

            history.push("/");

        }catch (error) {

            console.log(error.message)

        }

    }

    return (

            <React.Fragment>
                <NavBar/>

                <main className={classes.layout} style={{ marginTop: "5rem" }}>
                    <Paper className={classes.paper}>
                        <Typography component="h1" variant="h4" align="center" style={{ marginTop: '1rem' }}>
                           Contact Us
                        </Typography>
                        <Divider />

                        {/*<Typography component="h4" style={{ marginTop: '1.5rem', marginRight:'26rem' }}>Recepient Email</Typography>*/}
                        {/*<TextField id="recepientemail"*/}
                        {/*           fullWidth*/}
                        {/*           margin="normal"*/}
                        {/*           variant="outlined"*/}
                        {/*           placeholder="anishaparkar95@gmail.com"*/}
                        {/*           defaultValue="anishaparkar95@gmail.com"*/}
                        {/*/>*/}

                        <Typography component="h4"style={{ marginRight:'28rem' }}> Sender Email</Typography>
                        <TextField id="senderemail"
                                   multiline
                                   fullWidth
                                   margin="normal"
                                   variant="outlined"
                                   placeholder="From email"
                                   value ={senderEmail}
                                   onChange={(event => setSenderEmailID(event.target.value) )}

                        />

                        <Typography component="h4" style={{ marginRight:'28rem' }}>Email Subject</Typography>
                        <TextField id="outlined-multiline-static"
                                   multiline
                                   fullWidth
                                   margin="normal"
                                   variant="outlined"
                                   placeholder="Email Subject"
                                   value = {emailSubject}
                                   onChange={(event => setEmailSubject(event.target.value) )}

                        />

                        <Typography component="h4" style={{ marginRight:'29rem' }}>Email Text</Typography>
                        <TextField id="outlined-multiline-static"
                                   multiline
                                   fullWidth
                                   // defaultValue="Default Value"
                                   margin="normal"
                                   variant="outlined"
                                   placeholder="Email Text"
                                   value = {emailText}
                                   onChange={(event => setEmailText(event.target.value) )}
                        />

                        <Grid >
                            <Button type="submit"
                                    variant="contained"
                                    color="primary"
                                    // style={{ backgroundColor: 'black', marginTop: '1rem', }}
                                    onClick = {sendEmail}
                            >
                                Send Email
                            </Button>
                        </Grid>
                    </Paper>
                </main>
            </React.Fragment>

    );



}