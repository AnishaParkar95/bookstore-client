import React from 'react';
// import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import MainFeaturedPost from "./components/MainFeaturedPost";
import FeaturedPost from "./components/FeaturedPost";
// import Header from "./components/Header";
import NavBar from "./components/NavBar";
import Axios from "axios";



// const useStyles = makeStyles((theme) => ({
//     mainGrid: {
//         marginTop: theme.spacing(3),
//     },
// }));

// const sections = [
//     { title: 'Technology', url: '#' },
//     { title: 'Design', url: '#' },
//     { title: 'Culture', url: '#' },
//     { title: 'Business', url: '#' },
//     { title: 'Politics', url: '#' },
//     { title: 'Opinion', url: '#' },
//     { title: 'Science', url: '#' },
//     { title: 'Health', url: '#' },
//     { title: 'Style', url: '#' },
//     { title: 'Travel', url: '#' },
// ];

const mainFeaturedPost = {
    title: 'Find Your Great Reads! So Much Fun!',
    description:
        "Buy 1, Get 1 50% off, PDF Copies, Paperbacks, or Even Borrow the books! Check our Books of the Month and Explore!",
    image: 'https://libraries.ne.gov/northbend/files/2020/01/books-background.jpg',
    imgText: 'main image description',

};

// const featuredPosts = [
//     {
//         title: 'Becoming',
//         date: '$27.49',
//         description:
//             'An intimate, powerful, and inspiring memoir by the former First Lady of the United States.',
//         image: 'https://www.statefarmarena.com/assets/img/MichelleObama-590x590-StateFarm-56b2d1571b.jpg',
//         imageText: 'Image Text',
//     },
//     {
//         title: 'If It Bleeds',
//         date: '$21.00',
//         description:
//             'The Life of Chuck, Rat, and the title story If It Bleeds—each pulling you into intriguing and frightening places.',
//         image: 'https://www.gannett-cdn.com/presto/2020/04/19/USAT/d491252a-6bec-4691-a4a9-ef722290df56-IF_IT_BLEEDS_cover_image.jpg',
//         imageText: 'Image Text',
//     },
//     {
//         title: 'Before We Were Yours',
//         date: '$13.99',
//         description:
//             'Lisa Wingate takes an almost unthinkable chapter in our nation’s history and weaves a tale of enduring power.',
//         image: 'https://images-na.ssl-images-amazon.com/images/I/81rbuHYLsVL.jpg',
//         imageText: 'Image Text',
//     },
//     {
//         title: 'Oona Out Of Order',
//         date: '$20.99',
//         description:
//             'With its countless epiphanies and surprises, Oona proves difficult to put down. —USA Today',
//         image: 'https://images-na.ssl-images-amazon.com/images/I/91XvfMnR3gL.jpg',
//         imageText: 'Image Text',
//     },
//
// ];

export default function HomePage() {

    const [books, setBooks] = React.useState([]);

    React.useEffect(()=>{
        (async () => {
            try{

                const response = await Axios.get('/loadbooks');
                console.log("render home:", JSON.stringify(response.data))
                const { data} = response;
                setBooks(data);

            }catch(error){
                console.log("in home books loading error")
                console.error(error.message);
            }


        })();

    },[])

    // const classes = useStyles();

    return (
        <React.Fragment>

            <NavBar/>

            <CssBaseline />

            {/*<Header sections={sections} />*/}

            <Container maxWidth="lg" style={{marginTop:'0.1rem'}}>

                <main>
                    <MainFeaturedPost post={mainFeaturedPost} />

                    <Grid container spacing={4}>
                        {books.map((post,index) => (
                            <FeaturedPost key={index} post={post} />
                        ))}
                    </Grid>
                </main>

            </Container>
        </React.Fragment>
    );
}