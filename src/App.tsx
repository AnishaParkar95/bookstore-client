import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {LoginPage} from "./LoginPage";
import Registration from "./Registration";
import HomePage from "./HomePage";
import ContactUs from "./ContactUs";
import BookDetailPage from "./BookDetailPage";
import MyOrders from "./MyOrders";


const useStyles = makeStyles((myTheme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: myTheme.spacing(2),
            textAlign: 'center',
            color: myTheme.palette.text.secondary,
        },
    }),
);

function App() {

    const classes = useStyles();
    return (

        <Router>
            <div>
                <Switch>

        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>

                        <Route exact path="/">
                            <HomePage />
                        </Route>

                        <Route exact path = "/login">
                            <LoginPage/>
                        </Route>

                        <Route exact path = "/register">
                            <Registration/>
                        </Route>

                        <Route exact path = "/contactUs">
                            <ContactUs/>
                        </Route>

                        <Route exact path = "/bookdetail">
                            <BookDetailPage/>
                        </Route>

                        <Route exact path = "/myorder">
                           <MyOrders/>
                        </Route>


                    </Paper>
                </Grid>
            </Grid>
        </div>
                </Switch>
            </div>
        </Router>
);
}

export default App;
