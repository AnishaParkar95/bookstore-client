import React from 'react';
import NavBar from "./components/NavBar";
import Grid from "@material-ui/core/Grid";
import {Avatar} from "@material-ui/core";
import OrderDisplayPost from "./components/OrderDisplayPost";
import Axios from "axios";



export default function MyOrders() {

    const [orders, setOrders] = React.useState([]);

    React.useEffect(()=>{
        (async () => {
            try{

                const response = await Axios.get('/loadOrders');
                console.log("render orders:", JSON.stringify(response.data))
                const { data} = response;
                setOrders(data);

            }catch(error){
                console.log("in Orders, my orders loading error")
                console.error(error.message);
            }


        })();

    },[])

    return(

        <div >
            <NavBar />
            <div style={{ position: 'relative', overflow: 'hidden', width: '89rem', height: '17rem', marginTop: '0.5rem' }}>
                <img src="https://previews.123rf.com/images/victosha/victosha1808/victosha180800257/109425608-vector-illustration-of-horizontal-banner-of-bookshelves-with-retro-style-books.jpg" alt="" />
            </div>
            <Grid container justify="center" alignItems="center" style={{ marginTop: '-9rem', position: 'absolute' }}>
                <Avatar alt="Remy Sharp" src="https://cdn2.iconfinder.com/data/icons/circle-avatars-1/128/039_girl_avatar_profile_woman_headband-512.png" style={{ width: 210, height: 210, }} />
                <h1 style={{ marginTop: '8rem', position: 'absolute' }}> Anisha Parkar</h1>
            </Grid>
            <div style={{ marginTop: '8rem' }}>
                <Grid container spacing={4}>
                {orders.map((post,index) => (
                    <OrderDisplayPost key={index} post={post} />
                ))}
                </Grid>
            </div>

        </div>

    )

}