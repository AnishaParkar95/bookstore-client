import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Hidden from '@material-ui/core/Hidden';
// import BookDetailPage from "../BookDetailPage";
// import {Link} from "react-router-dom";
// import Button from "@material-ui/core/Button";
// import BookDetailPage from "../BookDetailPage";

const useStyles = makeStyles({
    card: {
        display: 'flex',
    },
    cardDetails: {
        flex: 1,
    },
    cardMedia: {
        width: 160,
    },
});

export default function FeaturedPost(props:any) {
    const classes = useStyles();
    const { post } = props;
    //
    //  function buttonHandler(){
    //
    //     console.log("hello i am here")
    //     return <BookDetailPage  />
    //
    // }

    return (
        <Grid item xs={12} md={6}>
            <CardActionArea component="a" href={"/bookdetail"} >

                {/*<Button onClick={buttonHandler}>*/}

                <Card className={classes.card}>
                    <div className={classes.cardDetails}>

                        <CardContent>
                            <Typography component="h2" variant="h5">
                                {post.bookName}
                            </Typography>
                            <Typography variant="subtitle1" color="textSecondary">
                                ${post.bookPrice}
                            </Typography>
                            <Typography variant="subtitle1" paragraph>
                                {post.description}
                            </Typography>
                            <Typography variant="subtitle1" color="primary">
                                Buy Now.
                            </Typography>

                        </CardContent>
                    </div>
                    <Hidden xsDown>
                        <CardMedia className={classes.cardMedia} image={post.image} title={post.imageTitle}/>
                    </Hidden>
                </Card>
                {/*</Button>*/}

            </CardActionArea>
        </Grid>
    );
}

FeaturedPost.propTypes = {
    post: PropTypes.object,
};