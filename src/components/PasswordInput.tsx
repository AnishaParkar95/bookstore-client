
import React from 'react';
import TextField from "@material-ui/core/TextField/TextField";

export default function PasswordInput() {

    return(
        <div>

            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
            />

        </div>

    );

}