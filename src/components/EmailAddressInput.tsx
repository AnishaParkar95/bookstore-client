import React from 'react';
import TextField from "@material-ui/core/TextField/TextField";

interface IEmailAddressInput {

    emailID: string,
    onEmailIDChange : any

}

export default function EmailAddressInput(props: IEmailAddressInput){

    const emailId = props.emailID;
    const onEmailIDChange = props.onEmailIDChange

    return(

        <div>

            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                // emailID = {emailId}
                // onEmailIdChange = {onEmailIDChange}
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus
            />

        </div>

    );


}