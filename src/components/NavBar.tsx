import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
// import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import { createStyles, fade, Theme, makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import Button from "@material-ui/core/Button";
import {useHistory} from "react-router";
// import { useSelector} from "react-redux";
// import {IRootReducer} from "../redux/IRootReducer";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import ShoppingCartRoundedIcon from '@material-ui/icons/ShoppingCartRounded';
// import {Link} from "react-router-dom";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
            marginRight: '30rem',
            display: 'none',
            [theme.breakpoints.up('sm')]: {
                display: 'inline',
            },
        },
        search: {
            position: 'relative',
            borderRadius: theme.shape.borderRadius,
            backgroundColor: fade(theme.palette.common.white, 0.15),
            '&:hover': {
                backgroundColor: fade(theme.palette.common.white, 0.25),
            },
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(1),
                width: 'auto',
            },
        },
        searchIcon: {
            padding: theme.spacing(0, 2),
            height: '100%',
            position: 'absolute',
            pointerEvents: 'none',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
        inputRoot: {
            color: 'inherit',
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                width: '12ch',
                '&:focus': {
                    width: '20ch',
                },
            },
        },
    }),
);

export default function NavBar() {

    const classes = useStyles();
    const history = useHistory();


    // const username: string | undefined = useSelector<IRootReducer, string | undefined>(
    //     state => state.usernameReducer.username);

    function handleLogin(){

        history.push('/login');
    }

    function handleHome(){

        history.push('/');
    }

    function handleCart(){

        history.push('/myorder');
    }

    function handleContactUs(){


        history.push('/contactUs');
    }

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    {/*<Link to= "/">*/}
                    {/*<Typography  variant="h6" className={classes.title}>*/}
                    {/*    Anisha's Book Store*/}
                    {/*</Typography>*/}
                    {/*</Link>*/}

                    <Button className={classes.title} color="inherit"
                            onClick={handleHome} >
                        Anisha's Book Store
                    </Button>
                    {/*{username}*/}

                    <div className={classes.search}>
                        <div className={classes.searchIcon}>
                            <SearchIcon />
                        </div>
                        <InputBase
                            placeholder="Search…"
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </div>
                    <Button style={{marginLeft:'1.5rem'}}color="inherit"
                            onClick={handleLogin} >
                        Login
                    </Button>
                    <AccountCircleIcon/>

                    <Button style={{marginLeft:'1.5rem'}}color="inherit"
                            onClick={handleCart} >
                        Orders
                    </Button>
                    <ShoppingCartRoundedIcon/>

                    <Button style={{marginLeft:'1.5rem'}}color="inherit"
                            onClick={handleContactUs} >
                        Contact Us
                    </Button>
                    <ContactMailIcon/>

                </Toolbar>
            </AppBar>
        </div>
    );
}